import shutil
import curses
import logging

class View:
    """Displays text to the user.

    Both the main TextBuffer and the status display are made visible by this class.

    Only supports 1 file and 1 status bar.
    """

    window_height = 0
    window_width = 0
    status_height = 1
    status_width = 0
    status_window = None
    status_error = False
    buffer_height = 0
    buffer_width = 0
    file_window = None

    def __init__(self):
        """Initialise the View."""
        self.window_width, self.window_height = shutil.get_terminal_size()

        # logging
        self.logger = logging.getLogger('pim')

        # Curses setup. Recommended by Curses devs.
        curses.initscr()
        curses.noecho()
        curses.cbreak()
        # Init curses colors
        curses.start_color()
        curses.use_default_colors()
        # Red text on white background
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_RED)

        self.status_height = 1
        self.status_width = self.window_width
        # Add a new window at the bottom, starting on the right
        self.status_window = curses.newwin(self.status_height,
                                           self.status_width,
                                           self.window_height - self.status_height,
                                           0)

        self.buffer_height = self.window_height - self.status_height
        self.buffer_width = self.window_width
        # By default, new window is added at top left corner
        self.file_window = curses.newwin(self.buffer_height, self.buffer_width)

        self.file_window.keypad(True)
        curses.beep()

    def close(self):
        """Gracefully close window.
        """

        curses.nocbreak()
        self.file_window.keypad(False)
        curses.echo()
        curses.endwin()

    def paint_status(self, input_text_buffer):
        """Paint a text buffer as the status bar.
        """
        text = str(input_text_buffer)
        color = 0
        if self.status_error:
            text = ' ' + text + ' '
            color = 1
            self.status_error = False

        self.status_window.move(0, 0)
        self.status_window.clrtoeol()
        self.status_window.addstr(0, 0, text, curses.color_pair(color))
        self.status_window.refresh()

    def clear_status(self):
        """Empty the status window.
        """

        self.status_window.move(0, 0)
        self.status_window.clrtoeol()
        self.status_window.refresh()

    def paint_file(self, input_text_buffer, end_line=-1):
        """Paint a text buffer as the file being edited.
        """
        max_y, _ = self.get_file_window_size()
        self.file_window.erase()
        curr_y = 0
        lines = [line for line in input_text_buffer.lines()]
        start_line = max_y
        for line in lines[-start_line:end_line]:
            self.file_window.addstr(curr_y, 0, line)
            curr_y += 1
        self.file_window.refresh()

    def get_file_input_char(self):
        """Return a new character from the file window.

        Returns:
            char: the next input character
        """
        return self.file_window.getch()

    def get_status_input_char(self):
        """Return a new character from the view window.

        Returns:
            char: the next input character
        """
        return self.status_window.getch()

    def get_file_window_size(self):
        return self.file_window.getmaxyx()

    def get_file_cursor_pos(self):
        y, x = self.file_window.getyx()
        return x, y

    def file_refresh(self):
        return self.status_window.refresh()

    def file_move(self, down_y, right_x):
        """Relative move; move the cursor down by `down_y` rows and right by
        `left_x` columns on the file window.
        """
        cursor_y, cursor_x = self.file_window.getyx()
        curr_y, curr_x = curses.getsyx()
        self.file_window.move(cursor_y + down_y, cursor_x + right_x)
        curses.setsyx(curr_y + down_y, curr_x + down_y)
        self.logger.info('moving file cursor to x: %d y: %d',
                         cursor_x + right_x,
                         cursor_y + down_y)
        curses.doupdate()
        self.file_window.refresh()

    def status_move(self, down_y, right_x):
        """Relative move; move the cursor down by `down_y` rows and right by
        `left_x` columns on the status window.
        """
        cursor_y, cursor_x = self.status_window.getsyx()
        self.status_window.move(cursor_y + down_y, cursor_x + right_x)
