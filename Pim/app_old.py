"""Main Pim execution.

Pim developed by Phil Denhoff, 2018"""

import shutil
import curses
from Pim.text_buffer import TextBuffer

def paint_status(status_text, status_bar_win):
    status_bar_win.move(0, 0)
    status_bar_win.clrtoeol()
    status_bar_win.addstr(0, 0, status_text)
    status_bar_win.refresh()

def paint_buffer(input_tb, term):
    term.erase()
    curr_y = 0
    for line in input_tb.split('\n'):
        term.addstr(curr_y, 0, line)
        curr_y = curr_y + 1

def write_mode(input_tb, term, status):
    _, max_x = term.getmaxyx()
    index = 0
    while True:
        term.refresh()
        input_char = term.getch()
        cursor_y, cursor_x = curses.getsyx()
        index = len(input_tb.text.split('\n')[:cursor_y]) + cursor_x
        # ESC
        if input_char == 27:
            break
        # Enter
        elif input_char == 10:
            input_tb.insert('\n', index + 1)
            index = index + 1
            cursor_y, _ = curses.getsyx()
            term.move(cursor_y + 1, 0)
            continue
        # Backspace
        elif input_char == 127:
            if index > 0:
                index = index - 1
                input_tb.delete_index(index)
                paint_buffer(input_tb.text, term)
            continue
        # down arrow
        elif input_char == 260:
            index = index - 1
            if cursor_x - 1 > 0:
                term.move(cursor_y, cursor_x - 1)
            continue
        # up arrow
        elif input_char == 261:
            if cursor_x + 1 < max_x \
                and index + 1 < len(input_tb.text) \
                and input_tb.text[index + 1] != '\n':
                index = index + 1
                term.move(cursor_y, cursor_x + 1)
            continue
        # right arrow
        elif input_char == 258:
            index = len(input_tb.text.split('\n')[:cursor_y + 1]) + cursor_x
            term.move(cursor_y + 1, cursor_x)
            continue
        # left arrow
        elif input_char == 259:
            index = len(input_tb.text.split('\n')[:cursor_y - 1]) + cursor_x
            term.move(cursor_y - 1, cursor_x)
            continue

        paint_status(str(input_char), status)
        input_tb.insert(chr(input_char), index)
        index = index + 1
        term.move(cursor_y, cursor_x + 1)
        paint_buffer(input_tb.text, term)
        term.refresh()
    return input_tb

def handle_status_input(status):
    input_string = ''
    index = 0
    while True:
        input_char = status.getch()
        if input_char == 10:
            break
        if input_char == 127:
            if index > 0:
                index = index - 1
                # input_string = input_string[:index] + input_string[index + 1:]
        input_string = input_string + chr(input_char)
        index = index + 1
        paint_status(':{}'.format(input_string), status)
    paint_status('', status)
    if input_string == 'w':
        # save the file
        pass
    elif input_string == 'q':
        return True

    return False

def main():
    columns, rows = shutil.get_terminal_size()
    curses.initscr()
    term = curses.newwin(rows - 1, columns)
    status = curses.newwin(1, columns, rows - 1, 0)

    curses.noecho()
    curses.cbreak()
    term.keypad(True)

    curses.beep()
    term.addstr('Terminal size: {} rows, {} cols'.format(rows, columns))
    term.refresh()

    input_tb = TextBuffer()
    while True:
        input_char = term.getch()
        if input_char == ord('i'):
            paint_status(' -- INSERT --', status)
            input_tb = write_mode(input_tb, term, status)
            paint_status(' -- NORMAL --', status)
        elif input_char == ord(':'):
            paint_status(':', status)
            should_quit = handle_status_input(status)
            if should_quit:
                break
        else:
            paint_status('Command {} not recognized'.format(chr(input_char)), status)

    curses.nocbreak()
    term.keypad(False)
    curses.echo()
    curses.endwin()

if __name__ == '__main__':
    main()
