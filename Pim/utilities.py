import logging
import sys

# create logger
MODULE_LOGGER = logging.getLogger('pim')

class Auxiliary:
    def __init__(self):
        self.logger = logging.getLogger('pim.Auxiliary')
        self.logger.info('creating a logger instance')
        logformat = '%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s'
        datefmt = '%m-%d %H:%M'

        logging.basicConfig(filename='app.log', level=logging.INFO, filemode='w',
                            format=logformat, datefmt=datefmt)
        stream_handler = logging.StreamHandler(sys.stderr)
        stream_handler.setFormatter(logging.Formatter(fmt=logformat, datefmt=datefmt))

        self.logger.addHandler(stream_handler)

    def do_something(self):
        self.logger.info('doing something')
        a = 1 + 1
        self.logger.info('done doing something')

def some_function():
    MODULE_LOGGER.info('received a call to "some_function"')