"""Main Pim execution.

Pim developed by Phil Denhoff, 2018"""

import logging
import argparse
import sys
from Pim.file_buffer import FileBuffer
from Pim.text_buffer import TextBuffer
from Pim.view import View

def parse_arguments():
    """Parse command-line arguments.

    Returns:
        Namespace: The arguments within a Namespace object. See argparse docs
        for more details.
    """

    parser = argparse.ArgumentParser(description='A vim-like text editor in Python.')
    parser.add_argument('file', type=str, nargs='?',
                        help='a file to load')

    return parser.parse_args()

class Pim:
    """A Pim session.
    """

    this_file = None
    file_name = None
    status_bar = None
    main_screen = None
    should_exit = False # True if we should exit on next loop

    def __init__(self):
        # parse command line arguments
        args = parse_arguments()


        self.main_screen = View()
        if args.file:
            try:
                self.this_file = FileBuffer(file_path=args.file)
            except Exception as err:
                print(err, file=sys.stderr)
                sys.exit(1)
        else:
            self.this_file = FileBuffer()
        self.status_bar = TextBuffer(None)

        # Set up logging
        self.logger = logging.getLogger('pim')
        self.logger.info('creating a Pim instance')
        logformat = '%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s'
        datefmt = '%m-%d %H:%M'

        logging.basicConfig(filename='app.log', level=logging.INFO, filemode='w',
                            format=logformat, datefmt=datefmt)

    def write_mode(self):
        """Main loop for entering write mode
        """
        self.this_file.handle_input_write_mode(self.main_screen)

    def save(self, input_name=None):
        """Write current text buffer this_file to disk
        """
        if self.file_name is None:
            if not input_name:
                self.status_bar.set_text('No file name')
                self.main_screen.status_error = True
                self.main_screen.paint_status(self.status_bar)
                return
            else:
                self.file_name = input_name

        with open(self.file_name, 'w+') as open_file:
            open_file.write(str(self.this_file))
            open_file.close()

    def handle_status_input(self):
        """Main loop for handling status input
        """
        input_string = ''
        index = 0

        # Get full input using loop
        while True:
            input_char = self.main_screen.get_status_input_char()

            if input_char == 10:
                break
            elif input_char == 127 or input_char == '^?':
                if index > 0:
                    index = index - 1
                    input_string = input_string[:index] + input_string[index + 1:]
            else:
                input_string += chr(input_char)
                index += 1

            self.status_bar.set_text(':' + input_string)
            self.main_screen.paint_status(self.status_bar)

        self.status_bar.set_text('')
        self.main_screen.paint_status(self.status_bar)
        if not input_string:
            return

        if input_string[0] == 'w':
            if len(input_string.split(' ')) > 1:
                self.save(input_string.split(' ')[1])
            else:
                self.save()
        elif input_string == 'q':
            self.should_exit = True

    def main(self):
        """Main interaction loop.
        """
        try:
            while True:
                if self.should_exit:
                    break
                self.main_screen.paint_file(self.this_file)
                max_x, max_y = self.main_screen.get_file_window_size()
                x, y = self.main_screen.get_file_cursor_pos()
                input_char = self.main_screen.get_file_input_char()

                if input_char == ord('i'):
                    self.status_bar.set_text('-- INSERT -- ')
                    self.main_screen.paint_status(self.status_bar)
                    self.write_mode()
                    self.status_bar.set_text('-- NORMAL -- ')
                    self.main_screen.paint_status(self.status_bar)
                    continue
                elif input_char == ord(':'):
                    self.status_bar.set_text(':')
                    self.main_screen.paint_status(self.status_bar)
                    self.handle_status_input()
                    continue
                elif input_char == 10:
                    # Enter
                    self.main_screen.clear_status()
                    continue
                elif input_char == 258:
                    # down arrow
                    if y + 1 < max_y:
                        self.main_screen.file_move(1, 0)
                    self.main_screen.paint_status(self.status_bar)
                    continue
                elif input_char == 259:
                    # up  arrow
                    if y - 1 > 0:
                        self.main_screen.file_move(-1, 0)
                    self.main_screen.paint_status(self.status_bar)
                    continue
                elif input_char == 261:
                    # right arrow
                    self.main_screen.paint_status(self.status_bar)
                    if x + 1 < max_x:
                        self.main_screen.file_move(0, 1)
                    continue
                elif input_char == 260:
                    # left arrow
                    self.main_screen.paint_status(self.status_bar)
                    if x - 1 > 0:
                        self.main_screen.file_move(0, -1)
                    continue
                else:
                    self.status_bar.set_text(
                        'Command {}|{} not recognized'
                        .format(
                            chr(input_char),
                            input_char)
                        )
                    self.main_screen.paint_status(self.status_bar)
        finally:
            self.main_screen.close()

        exit(0)

def main():
    """Start app by entering a Pim class.
    """
    pim = Pim()
    # Enter main loop
    pim.main()
