"""File buffer. THis is the active text a user sees.

Copyright Phil Denhoff 2018
"""
import logging
from enum import Enum
from Pim.text_buffer import TextBuffer

class Mode(Enum):
    """Enumerable which describes the files current mode: either writable (where key
    input is directly added to the buffer), normal (commands), or visual (unsupported atm)
    """

    INSERT = 'Insert'
    NORMAL = 'Normal'
    VISUAL = 'Visual'

class FileBuffer(TextBuffer):
    """An open file in Pim; the FileBuffer extends the TextBuffer with file-specific
    actions, like saving.
    """

    mode = Mode.NORMAL
    index = 0

    def __init__(self, file_path=None):
        super().__init__(file_path)
        # Enable logging
        self.logger = logging.getLogger('pim')

    def index_from_coords(self, x, y):
        # pylint: disable=C0103
        """Convert X / Y coordinates into an index. Splits files on '\n' to
        determine how far along the file the index is. Includes newline character.

        Args:
            y (int): the y position of the cursor; 0 in the top row
            x (int): the x position of the cursor; 0 in the left column
        """
        total_length = 0
        for index, line in enumerate(self.lines()):
            if index >= y:
                break
            if line == '':
                total_length += 1
            else:
                total_length += len(line) + 1

        last_line = self.get_line(y)
        total_length += len(last_line[:x])

        return total_length

    def handle_input(self, input_view):
        input_view.paint_file(self)
        if self.mode == Mode.INSERT:
            self.handle_input_write_mode(input_view)

    def handle_input_write_mode(self, input_view):
        """Handle input in write mode: this should directly append characters
        to the text buffer.

        Args:
            input_view (View): The view object, giving us access to the file
            and status buffers (so we can read characters).
        """
        # Loop until we exit write mode
        while True:
            input_char = input_view.get_file_input_char()
            max_x, max_y = input_view.get_file_window_size()
            x, y = input_view.get_file_cursor_pos()
            index = self.index_from_coords(x, y)

            if input_char == 27:
                # Esc
                break
            elif input_char == 10:
                # Enter
                self.insert('\n', index)
                input_view.file_move(1, 0)
            elif input_char == 127:
                # Backspace
                if index > 0:
                    self.delete_index(index - 1)
                    # input_view.file_move(0, -1)
            elif input_char == 260:
                # down arrow
                if y + 1 < max_y and y + 1 <= len(self.split_text):
                    self.logger.info('Going down!')
                    input_view.file_move(1, 0)
            elif input_char == 261:
                # up arrow
                if y - 3 > 0:
                    self.logger.info('Going up!')
                    input_view.file_move(-3, 0)
            # right arrow
            elif input_char == 258:
                if x + 1 > max_x and x + 1 >= len(self.get_line(y)):
                    self.logger.info('Going right!')
                    input_view.file_move(0, 1)
            # left arrow
            elif input_char == 259:
                if x - 1 > 0:
                    self.logger.info('Going left!')
                    input_view.file_move(0, -1)
            else:
                # regular character
                self.insert(chr(input_char), index)
                input_view.file_move(0, 1)

            input_view.paint_file(self)

    def handle_input_normal_mode(self, input_view):
        """Handle input in normal mode: we'll process commands in the file window.

        Args:
            input_view (View): The view object, giving us access to the file
            and status buffers (so we can read characters).
        """
        # Loop until we exit write mode
        input_view.paint_status('🤔')
        while True:
            input_char = input_view.get_file_input_char()
            x, y = input_view.get_file_cursor_pos()
            max_x, max_y = input_view.get_file_window_size()

            input_view.paint_status('🤔')

            # down arrow
            if input_char == 260:
                if y + 1 < max_y:
                    input_view.file_move(1, 0)
                continue
            # up arrow
            if input_char == 261:
                if y - 1 > 0:
                    input_view.file_move(-1, 0)
                continue
            # right arrow
            if input_char == 258:
                if x + 1 > max_x:
                    input_view.file_move(0, 1)
                continue
            # left arrow
            if input_char == 259:
                if x - 1 > 0:
                    input_view.file_move(0, -1)
                continue

            input_view.paint_file(self)
            input_view.paint_status('🤔')
