"""Pim Text Buffer.

Copyright Phil Denhoff 2018
"""
import logging
import os

class TextBuffer():
    """Text buffer. Stores text of a file-like object. Can be written to disk.
    """

    split_text = [''] # primary data model
    file_name = ''

    cached_joined_text = None # split_text, joined by '\n'

    def __init__(self, file_path=None):
        """Initialise the text buffer.
        Args:
            file_name (string, optiona): Defaults to None. The path to an existing file,
        #     or the path to the new file.

        Raises:
            ValueError: if file_path does not represent a file.
        """
        self.logger = logging.getLogger('pim')

        if file_path:
            if not os.path.isfile(file_path):
                raise ValueError('Input path should be a file')

            self.file_name = os.path.basename(file_path)
            with open(file_path) as file_object:
                text = file_object.read()
            self.split_text = text.split('\n')

    def __repr__(self):
        """Return the full text of the buffer."""
        if self.cached_joined_text is None:
            self.cached_joined_text = '\n'.join(self.split_text)
        return self.cached_joined_text

    def get_text_from_index(self, start_index, end_index=-1):
        """Return the text from start_index to start_index + size, including newlines.

        If size = -1 or isn't included, return the text from start_index to the end.
        Args:
            start_index   (int)       the beginning index. Must be non-negative.
            size            (int, opt)  the number of chars to return, 0 indexed

        Returns:
            string      the subset of the buffer's text
        """
        if self.cached_joined_text is None:
            self.cached_joined_text = '\n'.join(self.split_text)

        if start_index < 0:
            raise IndexError('start_index must be non-negative')
        if start_index > len(self.cached_joined_text):
            raise IndexError('start_index out of bounds')
        if end_index < 0 and (len(self.cached_joined_text) + end_index + start_index) < start_index:
            raise IndexError('start_index and end_index must have a positive difference')

        newline_count = self.cached_joined_text.count('\n', 0, end_index)
        if end_index < 0 and end_index + newline_count >= 0:
            end_index = -1
        elif end_index < 0:
            end_index += newline_count - 1
        else:
            end_index += newline_count
        return self.cached_joined_text[start_index:end_index]

    def delete_index(self, index):
        """Delete the charater from the text at the location `index`.

        Args:
            index (int): a 1D int (0...n) that represents the location from
                the text where the character should be removed.

        Raises:
            ValueError: Raised if `index` is out of bounds.
        """
        if self.cached_joined_text is None:
            self.cached_joined_text = '\n'.join(self.split_text)

        if index < 0 or index > len(self.cached_joined_text):
            raise ValueError('Variable "index" is out of bounds')
        self.cached_joined_text = self.cached_joined_text[:index] +\
                                  self.cached_joined_text[index + 1:]
        self.split_text = self.cached_joined_text.split('\n')

    def delete_line(self, line_num):
        """Removes the line at `line_num` from the text.

        Args:
            line_num (int): the zero-indexed line number to remove.

        Raises:
            ValueError: if `line_num` is out-of-bounds.
        """

        if line_num < 0 or line_num > len(self.split_text):
            raise ValueError('Variable "line_num" is out of bounds')
        self.split_text = self.split_text[:line_num] + self.split_text[line_num + 1:]
        self.cached_joined_text = None

    def replace_index(self, index, char):
        """Replace the char at `index` with `char.

        Args:
            index (int): the zero-indexed index to replace
            char (string): the character to replace with
        """
        if len(char) > 1:
            raise ValueError('char should be only 1 character.')

        self.delete_index(index)
        self.insert(char, index)
        self.split_text = self.cached_joined_text.split('\n')

    def replace_line(self, line_num, line):
        """Replace one line with another.

        Args:
            line_num (int): a zero-indexed line number to replace.
            line (string): the text to replace the line with.
        """
        if line_num < 0 or line_num > len(self.split_text):
            raise ValueError('Variable "line_num" is out of bounds')

        self.split_text[line_num] = line
        self.cached_joined_text = None

    def search(self, regex_pattern):
        """Search the buffer for a regex pattern using a generator.

        Args:
            regex_pattern (raw string): a raw string which represents a regex pattern

        Returns:
            (int, int, text): the first index of each matching pattern, the
                size of the match, and the text itself.
        """

        pass
        # return index of first character of line if in text
        # otherwise, none

    def insert(self, char, index):
        """Add `char` to self.raw_text at the 1-dimensional index `index`.

        Args:
            char (chr): the character to add, as a chr
            index (int): the 1D spot along self.raw_text to insert the character
        """
        if self.cached_joined_text is None:
            self.cached_joined_text = '\n'.join(self.split_text)

        self.cached_joined_text = self.cached_joined_text[:index] +\
                                  str(char) +\
                                  self.cached_joined_text[index:]
        self.split_text = self.cached_joined_text.split('\n')
        self.logger.info(self.split_text)

    def set_text(self, text):
        """Set the internal text to arbitrary text.

        Args:
            text (string): The string to set the buffer to.
        """
        self.split_text = text.split('\n')
        self.cached_joined_text = text

    def lines(self):
        """Return the text buffer as a generator, splitting on each '\\n'.
        """
        for line in self.split_text:
            yield line

    def get_line(self, line_number):
        """Return the line from the raw text at zero-indexed `line_number`.

        Args:
            line_number (int): the zero-indexed line number to return

        Returns:
            String: the requested line
        """
        if self.split_text == []:
            return ''
        return self.split_text[line_number]

    def handle_input(self, input_view):
        """This should handle any input into the window.

        Args:
            input_view (View): The View object from a pparent class.

        Raises:
            NotImplementedError: inherited from the super class.
        """

        raise NotImplementedError('This method must be implemented!')
