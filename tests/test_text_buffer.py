""" Test the pim TextBuffer class.
"""
import unittest
import tempfile
import os
import Pim.text_buffer as tb

class TestTextBuffer(unittest.TestCase):
    """ Testing the TextBuffer class.
    """
    test_buffer = None

    def setUp(self):
        """Set up the text buffer.
        """
        self.tb_buffer = tb.TextBuffer()

    def test___init__(self):
        """Test that __init__() sets the file_name and text.

        Only if those values are passed in.
        """
        init_temp_file = tempfile.NamedTemporaryFile()
        init_temp_file.write(b'Hello world!\nHow are you?')
        init_temp_file.seek(0)
        expected_text_array = ['Hello world!', 'How are you?']
        self.tb_buffer = tb.TextBuffer(file_path=init_temp_file.name)
        self.assertEqual(self.tb_buffer.file_name, os.path.split(init_temp_file.name)[1])
        self.assertEqual(self.tb_buffer.split_text, expected_text_array)
        init_temp_file.close()

    def test___repr__(self):
        """Test that __repr__() returns the unmodified text of the buffer.
        """
        # Watch out, the last char is a Zero-width Joiner
        text_1 = ['difficult', 'to_😀☺😓😈​']
        self.tb_buffer.split_text = text_1
        self.tb_buffer.cached_joined_text = None
        self.assertEqual(str(self.tb_buffer), '\n'.join(text_1))

        text_2 = ['٩(-̮̮̃-̃)۶ ٩(●̮̮̃•̃)۶ ٩(͡๏̯͡๏)۶ ٩(-̮̮̃•̃).']
        self.tb_buffer.split_text = text_2
        self.tb_buffer.cached_joined_text = None
        self.assertEqual(str(self.tb_buffer), '\n'.join(text_2))

        text_3 = ['Testing «ταБЬℓσ»: 1<2 & 4+1>3, now 20% off!']
        self.tb_buffer.split_text = text_3
        self.tb_buffer.cached_joined_text = None
        self.assertEqual(str(self.tb_buffer), '\n'.join(text_3))

    def test_get_text_from_index(self):
        """Test get_text_from_index() returns the correct subset of `text`.
        """
        self.tb_buffer.split_text = ['hello', 'world!']

        # Testing with a couple lines; return the last line
        self.assertEqual(self.tb_buffer.get_text_from_index(5, -1),
                         '\nworld')

        # Testing with a couple lines; return 3 chars from line 1
        self.assertEqual(self.tb_buffer.get_text_from_index(0, 3),
                         'hel')

        # Size is larger than last index location
        self.assertEqual(self.tb_buffer.get_text_from_index(0, 30),
                         'hello\nworld!')

        # size is less than 0
        self.assertEqual(self.tb_buffer.get_text_from_index(0, -5),
                         'hello\nw')

        # request covers 2 lines
        self.assertEqual(self.tb_buffer.get_text_from_index(3, 6),
                         'lo\nw')

    def test_get_text_from_index__invalid(self):
        """Verify that get_text_from_index() raises exceptions for invalid params.
        """
        # invalid start_index; too low
        self.tb_buffer.split_text = ['hello', 'world!']
        with self.assertRaises(IndexError):
            self.tb_buffer.get_text_from_index(-1, 0)
        # invalid start_index; too high
        with self.assertRaises(IndexError):
            self.tb_buffer.get_text_from_index(55, 0)
        # invalid size; effectively requesting negative chars
        with self.assertRaises(IndexError):
            self.tb_buffer.get_text_from_index(5, -15)

    def test_delete_index(self):
        """Test that delete_index() deletes the char at `index`.
        """
        self.tb_buffer.split_text = ['123445']
        self.tb_buffer.delete_index(3)
        self.assertEqual(str(self.tb_buffer), '12345')

    def test_delete_index__out_of_bounds(self):
        """Test that delete_index() raises an exception for out-of-bound indexes.
        """
        self.tb_buffer.raw_text = 'one two'
        with self.assertRaises(ValueError):
            self.tb_buffer.delete_index(8)
        with self.assertRaises(ValueError):
            self.tb_buffer.delete_index(-1)

    def test_delete_line(self):
        """Test nominal behaviour of delete_line()
        """
        self.tb_buffer.split_text = ['hello', 'there', 'world']
        self.tb_buffer.delete_line(1)
        self.assertEqual(str(self.tb_buffer), 'hello\nworld')

    def test_delete_line__out_of_bounds(self):
        """Test delete_line() with line indexes that are out-of-bounds.
        """
        self.tb_buffer.split_text = ['hello', 'there', 'world']
        with self.assertRaises(ValueError):
            self.tb_buffer.delete_line(-1)
        with self.assertRaises(ValueError):
            self.tb_buffer.delete_line(5)

    def test_replace_index(self):
        """Test replacing an index with a new character.
        """
        # Simple test
        self.tb_buffer.set_text('0123%5')
        self.tb_buffer.replace_index(4, '4')
        self.assertEqual(str(self.tb_buffer), '012345')

    def test_replace_line(self):
        """Test replacing an entire line with another.
        """
        self.tb_buffer.set_text('hello\nthere\nearth')
        self.tb_buffer.replace_line(2, 'world')
        self.assertEqual(str(self.tb_buffer), 'hello\nthere\nworld')

    def test_search(self):
        """Test searching the buffer for a piece of text.
        """

        # Method to test has not been implemented
        pass

    def test_insert(self):
        """Test nominal use of the insert() method.
        """
        # One character insert
        self.tb_buffer.insert('a', 0)
        self.assertEqual(str(self.tb_buffer), 'a')

        # Multi word insert
        self.tb_buffer.split_text = ''
        self.tb_buffer.cached_joined_text = None
        string1 = 'The quick brown fox jumped over the lazy dog.'
        for index, char in enumerate(string1):
            self.tb_buffer.insert(char, index)
        self.assertEqual(str(self.tb_buffer), string1)

        # Out-of-order insert
        self.tb_buffer.split_text = ''
        self.tb_buffer.cached_joined_text = None
        string2 = 'hello'
        string3 = 'world'
        for index, char in enumerate(string3):
            self.tb_buffer.insert(char, index)
        self.tb_buffer.insert(' ', 0)
        for index, char in enumerate(string2):
            self.tb_buffer.insert(char, index)
        self.assertEqual(str(self.tb_buffer), 'hello world')

        # in-order multiline
        self.tb_buffer.split_text = ''
        self.tb_buffer.cached_joined_text = None
        string2 = 'hello'
        string3 = '\n'
        string4 = 'world'
        offset = 0
        for index, char in enumerate(string2):
            self.tb_buffer.insert(char, index)
        offset += len(string2)

        for index, char in enumerate(string3):
            self.tb_buffer.insert(char, index + offset)
        offset += len(string3)

        for index, char in enumerate(string4):
            self.tb_buffer.insert(char, index + offset)
        self.assertEqual(str(self.tb_buffer), 'hello\nworld')

    def test_insert__index_out_of_bounds(self):
        """Test insert() with index that is out of bounds.

        Expect anything past len(raw_text) to go to the end, less than 0 should
        be treated just like array subset; wrap around
        """
        # Test of index < 0. Should insert regularly
        self.tb_buffer.cached_joined_text = 'hello orld'
        self.tb_buffer.insert('w', -4)
        self.assertEqual(str(self.tb_buffer), 'hello world')

        # Test index > len(raw_text)
        self.tb_buffer.cached_joined_text = 'hello worl'
        self.tb_buffer.insert('d', 50)
        self.assertEqual(str(self.tb_buffer), 'hello world')

    def test_set_text(self):
        """Test setting the text all at once, instead of inserting.
        """
        # Simple one-liner
        text1 = 'Hello world!'
        self.tb_buffer.set_text(text1)
        self.assertEqual(str(self.tb_buffer), text1)

        # Multi-line
        text2 = '0\n1\n2\n3\n'
        text2_arr = ['0', '1', '2', '3', '']
        self.tb_buffer.set_text(text2)
        self.assertEqual(str(self.tb_buffer), text2)
        self.assertEqual(self.tb_buffer.split_text, text2_arr)

    def test_lines(self):
        """Test the lines generator; should return a list of all lines
        (lines are separated by '\n')
        """
        # Test without pre-generated split_text
        expected_text = ['four', 'next', 'last']
        self.tb_buffer.split_text = expected_text
        output = [line for line in self.tb_buffer.lines()]
        self.assertEqual(expected_text, output)

    def test_get_line(self):
        """Test get_line() for nominal use (both with and w/o prerendered
        split text. )
        """
        # Simple test, just using indexes
        self.tb_buffer.split_text = ['0', '1', '2', '3', '4', '5']
        for i in range(6):
            line = self.tb_buffer.get_line(i)
            self.assertEqual(line, str(i))

        # Sample simple test
        self.tb_buffer.split_text = ['hello', 'world']
        self.assertEqual(self.tb_buffer.get_line(0), 'hello')
        self.assertEqual(self.tb_buffer.get_line(1), 'world')

        # Pre-rendered split_text
        self.tb_buffer.split_text = ['My name', 'smells like', 'it spells']
        self.assertEqual(self.tb_buffer.get_line(-1), 'it spells')

    def test_get_line__empty_split_text(self):
        """Test get_line() if the split_text array is empty.
        """
        self.tb_buffer.split_text = []
        self.assertEqual(self.tb_buffer.get_line(0), '')

    # Testing composed modules

    def test_lines__after_insert(self):
        """Test the output of lines() after using insert() to insert a string.
        """
        # single line
        sample_text = 'hello world'
        for index, char in enumerate(sample_text):
            self.tb_buffer.insert(char, index)
        first_line = next(self.tb_buffer.lines())
        self.assertEqual(sample_text, first_line)

        # multiline, 2 lines
        self.tb_buffer.cached_joined_text = None
        self.tb_buffer.split_text = ''
        sample_text = 'hello world\nhow are you?'
        for index, char in enumerate(sample_text):
            self.tb_buffer.insert(char, index)
        for index, line in enumerate(self.tb_buffer.lines()):
            self.assertEqual(sample_text.split('\n')[index], line)

        # multiline, 5 lines
        self.tb_buffer.cached_joined_text = None
        self.tb_buffer.split_text = ''
        sample_text = 'hello\nworld\nhow\nare\nyou?'
        for index, char in enumerate(sample_text):
            self.tb_buffer.insert(char, index)
        lines = [line for line in self.tb_buffer.lines()]
        self.assertEqual('hello', lines[0])
        self.assertEqual('world', lines[1])
        self.assertEqual('how', lines[2])
        self.assertEqual('are', lines[3])
        self.assertEqual('you?', lines[4])

if __name__ == '__main__':
    unittest.main()
