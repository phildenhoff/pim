""" Test the pim FileBuffer class.
"""
import unittest
import unittest.mock as mock
import Pim.file_buffer as fb

class TestFileBuffer(unittest.TestCase):
    """Testing the File Buffer class.
    """
    test_file = None

    def setUp(self):
        """
        Set up the file buffer for each test.
        """
        self.test_file = fb.FileBuffer()

    def test_index_from_coords(self):
        """Test that converting coordinates to indicdes works for valid y and x
        """
        # two full lines
        self.test_file.set_text('four\nnext line')
        self.assertEqual(self.test_file.index_from_coords(0, 0), 0)
        self.assertEqual(self.test_file.index_from_coords(3, 0), 3)
        self.assertEqual(self.test_file.index_from_coords(0, 1), 5)
        self.assertEqual(self.test_file.index_from_coords(2, 1), 7)
        self.assertEqual(self.test_file.get_text_from_index(7, -6), 'x')
        self.test_file.split_text = None

        # one empty line
        self.test_file.set_text('four\n')
        self.assertEqual(self.test_file.index_from_coords(4, 0), 4)

    def test_index_from_coords__many_lines(self):
        """Test index_from_coords for a text file with many lines.
        """
        self.test_file.set_text('1\n2\n3\n4\n5\n6')
        self.assertEqual(self.test_file.index_from_coords(0, 5), 10)
        self.test_file.split_text = None

    def test_index_from_coords__empty_line(self):
        """Test index_from_coords with a line that is empty.
        """
        self.test_file.set_text('0\n')
        index1 = self.test_file.index_from_coords(0, 1)
        self.assertEqual(index1, 2)
        self.test_file.set_text('0\n1\n2\n3\n4')
        index = self.test_file.index_from_coords(0, 4)
        self.assertEqual(index, 8)

    def test_index_from_coords__invalid_x(self):
        """Test invalid x coord for index_form_coords.
        Expect the have x by limited to line width.
        """
        self.test_file.set_text('hello\nworld')
        self.assertEqual(self.test_file.index_from_coords(6, 1), 11)

    def test_handle_input_write_mode(self):
        """Test input_write_mode handling of Enter
        """
        view = mock.Mock()
        view.paint_file.return_value = True
        view.get_file_input_char.side_effect = [10, 27]
        view.get_file_cursor_pos.return_value = (5, 1)

        self.test_file.set_text('hello\nworld')
        self.test_file.handle_input_write_mode(view)
        self.assertTrue(view.file_move.has_been_called())
        print(self.test_file.split_text)
        self.assertEqual(len(self.test_file.split_text), 3)

        self.assertEqual(self.test_file.get_line(0), 'hello')
        self.assertEqual(self.test_file.get_line(1), 'world')
        self.assertEqual(self.test_file.get_line(2), '')


if __name__ == '__main__':
    unittest.main()
